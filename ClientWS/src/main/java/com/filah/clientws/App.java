package com.filah.clientws;

import com.filah.clientws.beans.Livro;
import com.filah.clientws.service.ServicePort;
import com.filah.clientws.service.interfaces.LivrosService;
import java.util.List;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        try{
            LivrosService service = new ServicePort().getLivrosService();
            List<Livro> livros = service.listarLivros();    

            for(Livro livro : livros){
               System.out.println(livro.getTitulo());
            }
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
    }
}
