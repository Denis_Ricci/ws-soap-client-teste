package com.filah.clientws.service.adapter;


import com.filah.clientws.beans.Autor;
import javax.xml.bind.annotation.adapters.XmlAdapter;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Ricci
 */
public class AdaptadorAutores extends XmlAdapter<String, Autor> {

    @Override
    public String marshal(Autor autor) throws Exception {
        return autor.getNome();
    }

    @Override
    public Autor unmarshal(String autor) throws Exception {
        return new Autor(autor);
    }
}
