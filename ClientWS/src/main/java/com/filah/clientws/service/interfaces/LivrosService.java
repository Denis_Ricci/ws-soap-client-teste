/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.filah.clientws.service.interfaces;

import com.filah.clientws.beans.EBook;
import com.filah.clientws.beans.Livro;
import java.util.List;
import javax.jws.WebMethod;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.ws.Action;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

/**
 *
 * @author Ricci
 */
@WebService(name = "LivrosService", targetNamespace = "http://servicos.webserviceteste.filah.com/")
public interface LivrosService {

    @WebMethod
    @WebResult(name = "livro", targetNamespace = "")
    @RequestWrapper(localName = "listarLivros", targetNamespace = "http://servicos.webserviceteste.filah.com/", className = "com.filah.webserviceteste.servicos.ListarLivros")
    @ResponseWrapper(localName = "listarLivrosResponse", targetNamespace = "http://servicos.webserviceteste.filah.com/", className = "com.filah.webserviceteste.servicos.ListarLivrosResponse")
    @Action(input = "http://servicos.webserviceteste.filah.com/LivrosService/listarLivrosRequest", output = "http://servicos.webserviceteste.filah.com/LivrosService/listarLivrosResponse")
    public List<Livro> listarLivros();

    public Livro listarLivroIndice(int index);

    public void criaLivro(Livro livro);

    public List<EBook> listarEbooks();
}
