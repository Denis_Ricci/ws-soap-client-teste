/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.filah.clientws.service;

import com.filah.clientws.service.interfaces.LivrosService;
import java.net.MalformedURLException;
import java.net.URL;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;

/**
 *
 * @author Ricci
 */
public class ServicePort extends Service{
    private static final QName SERVICENAME = new QName("http://servicos.webserviceteste.filah.com/", "LivrosServiceService");
    private static URL WSDL_LOCATION = null; 
    
    static{
        try{
            WSDL_LOCATION = new URL("http://localhost:8085/WebServiceTeste/livros?wsdl");
        }catch(MalformedURLException e){
            System.out.println("Erro ao instanciar URL " + e.getMessage());
        }
    }
    
    
    public ServicePort(){
        super(WSDL_LOCATION,SERVICENAME);
    }
    
    public LivrosService getLivrosService(){
        return super.getPort(new QName("http://servicos.webserviceteste.filah.com/","LivrosServicePort"),LivrosService.class);
    }
    
    
}
